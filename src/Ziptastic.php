<?php namespace Ziptastic;

class Ziptastic
{
    const ZIPTASTIC_LOOKUP_URL = 'https://zip.getziptastic.com/v3/%s/%d';

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var string
     */
    private $countryCode;

    /**
     * @param  string  $apiKey    API Key (For non-free accounts)
     */
    public function __construct($apiKey = null, $countryCode = 'US')
    {
        $this->apiKey = $apiKey;
        $this->countryCode = $countryCode;
    }

    public function lookup($zipCode)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, sprintf(self::ZIPTASTIC_LOOKUP_URL, $this->countryCode, $zipCode));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            sprintf("x-key: %s", $this->apiKey)
        ]);

        $response = curl_exec($ch);

        $res = json_decode(trim($response), true);
        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new Exception('Could not parse response as json');
        }

        if ($statusCode !== 200) {
            throw new Exception($res['message']);
        }

        $collection = [];
        foreach ($res as $result) {
            $collection[] = new LookupModel($result);
        }

        return $collection;
    }
}